/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu.presenter

import id.co.next_innovation.okfood.ui.base.presenter.BasePresenter
import id.co.next_innovation.okfood.ui.menu.interactor.MenuMvpInteractor
import id.co.next_innovation.okfood.ui.menu.view.MenuView
import id.co.next_innovation.okfood.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MenuPresenter<V: MenuView, I: MenuMvpInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) :
        BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable),
        MenuMvpPresenter<V, I> {

    override fun getLat(): Double {
        return interactor.let {
            it!!.getLat()
        }
    }

    override fun getLon(): Double {
        return interactor.let {
            it!!.getLon()
        }
    }

    override fun getCategoriesMenuRestaurant(restaurantId: Int) {
        getView()?.showProgress()
        interactor?.let {
            compositeDisposable.add(it.getCategoriesMenuRestaurant(restaurantId)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ categoriesResponse ->
                        if (categoriesResponse.data.categories.isNotEmpty()) {
                            getView().let {
                                it?.hideProgress()
                                //it?.showCategoriesOnTabs(categoriesResponse.data.categories)
                                it?.offScreenPageLimit(categoriesResponse.data.categories.size)
                                for (category in categoriesResponse.data.categories) {
                                    getView()?.addTab(category)
                                }
                            }
                        }
                    }))

        }
    }
}
