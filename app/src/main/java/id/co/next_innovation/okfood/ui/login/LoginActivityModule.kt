package id.co.next_innovation.okfood.ui.login

import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.login.interactor.LoginInteractor
import id.co.next_innovation.okfood.ui.login.interactor.LoginMvpInteractor
import id.co.next_innovation.okfood.ui.login.presenter.LoginMvpPresenter
import id.co.next_innovation.okfood.ui.login.presenter.LoginPresenter
import id.co.next_innovation.okfood.ui.login.view.LoginView
import javax.inject.Inject

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

@Module
class LoginActivityModule {

    @Provides
    internal fun provideLoginInteractor(loginInteractor: LoginInteractor): LoginMvpInteractor = loginInteractor

    @Provides
    internal fun provideLoginPresenter(loginPresenter: LoginPresenter<LoginView, LoginMvpInteractor>):
            LoginMvpPresenter<LoginView, LoginMvpInteractor> = loginPresenter
}