/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.restaurant.presenter

import id.co.next_innovation.okfood.ui.base.presenter.MVPPresenter
import id.co.next_innovation.okfood.ui.restaurant.interactor.RestaurantMvpInteractor
import id.co.next_innovation.okfood.ui.restaurant.view.RestaurantView

interface RestaurantMvpPresenter<V: RestaurantView, I: RestaurantMvpInteractor> : MVPPresenter<V, I> {

    fun getLat(): Double
    fun getLon(): Double
    fun onViewPrepared()
}