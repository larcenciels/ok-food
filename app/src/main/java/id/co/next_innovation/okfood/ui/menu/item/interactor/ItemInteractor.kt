package id.co.next_innovation.okfood.ui.menu.item.interactor

import id.co.next_innovation.okfood.data.network.ApiHelper
import id.co.next_innovation.okfood.data.network.reponse.MenuResponse
import id.co.next_innovation.okfood.data.preferences.PreferenceHelper
import id.co.next_innovation.okfood.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

class ItemInteractor  @Inject internal constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper),
        ItemMvpInteractor {

    override fun getRestaurantMenus(categoryId: Int): Observable<MenuResponse> = apiHelper.getMenuByCategoryId(categoryId)
}