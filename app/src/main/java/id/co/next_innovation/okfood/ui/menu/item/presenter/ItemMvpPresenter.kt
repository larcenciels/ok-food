package id.co.next_innovation.okfood.ui.menu.item.presenter

import id.co.next_innovation.okfood.ui.base.presenter.MVPPresenter
import id.co.next_innovation.okfood.ui.menu.item.interactor.ItemMvpInteractor
import id.co.next_innovation.okfood.ui.menu.item.view.ItemView

interface ItemMvpPresenter<V: ItemView, I: ItemMvpInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared(categoryId: Int)

}