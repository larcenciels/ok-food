package id.co.next_innovation.okfood.data.network

import id.co.next_innovation.okfood.data.network.reponse.*
import id.co.next_innovation.okfood.data.network.request.UserRequest
import io.reactivex.Observable

/**
 * Created by jyotidubey on 04/01/18.
 */
interface ApiHelper {

    fun performSignIn(request: UserRequest.Login) : Observable<UserResponse.Login>
    fun getGroupMenus() : Observable<GroupMenuResponse>
    fun getTypes() : Observable<TypeResponse>
    fun getRestaurants(): Observable<RestaurantResponse>
    fun getMenuCategoriesByRestaurantId(restaurantId: Int): Observable<CategoryResponse>
    fun getMenuByCategoryId(categoryId: Int) : Observable<MenuResponse>

}