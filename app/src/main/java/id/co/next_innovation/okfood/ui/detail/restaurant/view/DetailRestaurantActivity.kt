/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.detail.restaurant.view

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.data.database.model.Restaurant
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.detail.restaurant.interactor.DetailRestaurantMvpInteractor
import id.co.next_innovation.okfood.ui.detail.restaurant.presenter.DetailRestaurantMvpPresenter
import kotlinx.android.synthetic.main.activity_detail_restaurant.*
import kotlinx.android.synthetic.main.food_map_view.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class DetailRestaurantActivity : BaseActivity(), DetailRestaurantView, OnMapReadyCallback {

    @Inject
    lateinit var presenter: DetailRestaurantMvpPresenter<DetailRestaurantView, DetailRestaurantMvpInteractor>
    @Inject
    lateinit var dayAdapter: DayAdapter
    @Inject
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var restaurant: Restaurant
    private var mMap: GoogleMap? = null
    lateinit var latlng: LatLng
    lateinit var marker: MarkerOptions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_restaurant)
        presenter.onAttach(this)

        setup()

        setupMap(savedInstanceState)

        button_call.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + restaurant.contact)
            startActivity(intent)
        }

    }

    fun setup() {
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayShowTitleEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.title = resources.getString(R.string.details)
        actionBar?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        restaurant = intent.getSerializableExtra("restaurant") as Restaurant
        text_merchant_name.text = restaurant.name
        text_merchant_phone.text = restaurant.contact
        text_merchant_address.text = restaurant.street.toString() + ", " + restaurant.district + ", " + restaurant.city
        text_merchant_description.text = restaurant.description
        latlng = LatLng(restaurant.latitude, restaurant.longitude)

        list_business_hour.layoutManager = linearLayoutManager
        list_business_hour.itemAnimator = DefaultItemAnimator()

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_WEEK, calendar.firstDayOfWeek)
        var dates: List<Date>? = null
        dates = ArrayList()
        for (i in 0..6) {
            calendar.add(Calendar.DAY_OF_WEEK, 1)
            dates.add(calendar.time)

        }
        dayAdapter.addDayToList(dates, restaurant.open+" - "+restaurant.close)
        list_business_hour.adapter = dayAdapter


    }

    fun setupMap(savedInstanceState: Bundle?) {
        mapview_food_map.onCreate(savedInstanceState)
        val supportMapFragment: SupportMapFragment = supportFragmentManager.findFragmentById(R.id.mapview_food_map) as SupportMapFragment
        supportMapFragment.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap?) {
        setUpMap(p0)
    }

    fun setUpMap(map: GoogleMap?) {
        mMap = map
        mMap?.addMarker(MarkerOptions().position(latlng).title(restaurant.name).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)))

        val location = CameraUpdateFactory.newLatLngZoom(latlng, 15.toFloat())
        mMap?.animateCamera(location)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_social, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}