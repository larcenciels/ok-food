/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu.item.view

import android.content.Intent
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.ui.menu.view.MenuActivity
import id.co.next_innovation.okfood.data.database.model.Menu
import id.co.next_innovation.okfood.util.extension.loadImage
import kotlinx.android.synthetic.main.adapter_menus.view.*
import kotlinx.android.synthetic.main.test.view.*


class ItemAdapter(private val menuList: MutableList<Menu>) : RecyclerView.Adapter<ItemAdapter.MenuViewHolder>() {

    override fun getItemCount() = this.menuList.size

    override fun onBindViewHolder(holder: MenuViewHolder, position: Int) = holder.let {
        it.clear()
        it.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewMenu: Int) = MenuViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.adapter_menus, parent, false))


    internal fun addMenuToList(menus: List<Menu>) {
        this.menuList.addAll(menus)
        notifyDataSetChanged()
    }

    inner class MenuViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun clear() {
            itemView.text_menu_item_title.text = ""

        }

        fun onBind(position: Int) {

            val menu: Menu = menuList[position]

            itemView.text_menu_item_title.text = menu.name
            itemView.text_menu_item_details.text = menu.description
            itemView.text_menu_item_price.text = menu.price
            itemView.img_merchant.loadImage(menu.image)

            itemView.img_merchant.setOnClickListener{
                val builder: AlertDialog.Builder = AlertDialog.Builder(itemView.context)
                        .setMessage(menu.name + " - " +menu.price)
                        .setView(itemView.about_logo)
                val factory: LayoutInflater = LayoutInflater.from(itemView.context)
                val view: View = factory.inflate(R.layout.test, null)
                view.about_logo.loadImage(menu.image)
                builder.setView(view)
                builder.show()

            }

            setItemClickListener(menu, menu.id, menu.name)
        }

        private fun setItemClickListener(menu: Menu?, id: Int?, name: String?) {
            itemView.setOnClickListener {
                id?.let {
//                    val intent = Intent(itemView.context, MenuActivity::class.java)
//                    intent.putExtra("restaurant_name", name)
//                    intent.putExtra("restaurant_id", id)
//                    itemView.context.startActivity(intent)
                }
            }
        }

    }


}