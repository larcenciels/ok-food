package id.co.next_innovation.okfood.ui.splash

import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.splash.interactor.SplashInteractor
import id.co.next_innovation.okfood.ui.splash.interactor.SplashMvpInteractor
import id.co.next_innovation.okfood.ui.splash.presenter.SplashMvpPresenter
import id.co.next_innovation.okfood.ui.splash.presenter.SplashPresenter
import id.co.next_innovation.okfood.ui.splash.view.SplashView

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

@Module
class SplashActivityModule {

    @Provides
    internal fun provideSplashInteractor(splashInteractor: SplashInteractor): SplashMvpInteractor = splashInteractor

    @Provides
    internal fun provideSplashPresenter(splashPresenter: SplashPresenter<SplashView, SplashMvpInteractor>)
            : SplashMvpPresenter<SplashView, SplashMvpInteractor> = splashPresenter
}