/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.network.reponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import id.co.next_innovation.okfood.data.database.model.Menu

data class DataMenu internal constructor(
        @SerializedName("menus")
        @Expose
        var menus: List<Menu>
)