/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.home.view.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.data.database.model.GroupMenu
import id.co.next_innovation.okfood.ui.restaurant.view.RestaurantActivity
import id.co.next_innovation.okfood.util.extension.loadImage
import kotlinx.android.synthetic.main.adapter_group_menu.view.*

class GroupMenuAdapter(private val groupMenuList: MutableList<GroupMenu>) : RecyclerView.Adapter<GroupMenuAdapter.GroupMenuViewHolder>() {

    override fun getItemCount() = this.groupMenuList.size

    override fun onBindViewHolder(holder: GroupMenuViewHolder, position: Int) = holder.let {
        it.clear()
        it.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = GroupMenuViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.adapter_group_menu, parent, false))


    internal fun addGroupMenuToList(menus: List<GroupMenu>) {
        this.groupMenuList.addAll(menus)
        notifyDataSetChanged()
    }

    inner class GroupMenuViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun clear() {
            itemView.group_menu_icon.setImageDrawable(null)
            itemView.group_menu_name.text = ""
        }

        fun onBind(position: Int) {

            val (id, name, icon) = groupMenuList[position]

            inflateData(id, name, icon)
            setItemClickListener(id, name)
        }

        private fun setItemClickListener(groupId: Int?, name: String?) {
            itemView.setOnClickListener {
                groupId.let {
                    try {
                        val intent = Intent(itemView.context, RestaurantActivity::class.java)
                        intent.putExtra("groupId", groupId)
                        intent.putExtra("name", name)
                        itemView.context.startActivity(intent)
                    }catch (e: Exception){

                    }
                }
            }
        }

        private fun inflateData(id: Int?, name: String?, icon: String?) {
            name?.let { itemView.group_menu_name.text = it }
            icon?.let {
                itemView.group_menu_icon.loadImage(it)
            }
        }
    }


}