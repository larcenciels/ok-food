package id.co.next_innovation.okfood.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import id.co.next_innovation.okfood.BuildConfig
import id.co.next_innovation.okfood.data.database.AppDatabase
import id.co.next_innovation.okfood.data.network.ApiHeader
import id.co.next_innovation.okfood.data.network.ApiHelper
import id.co.next_innovation.okfood.data.network.AppApiHelper
import id.co.next_innovation.okfood.data.preferences.AppPreferenceHelper
import id.co.next_innovation.okfood.data.preferences.PreferenceHelper
import id.co.next_innovation.okfood.di.ApiKeyInfo
import id.co.next_innovation.okfood.di.PreferenceInfo
import id.co.next_innovation.okfood.util.AppConstants
import id.co.next_innovation.okfood.util.SchedulerProvider
import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.data.database.repository.user.UserRepo
import id.co.next_innovation.okfood.data.database.repository.user.UserRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Singleton

/**
 * Created by jyotidubey on 05/01/18.
 */
@Module
class AppModule {

    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    internal fun provideAppDatabase(context: Context): AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, AppConstants.APP_DB_NAME).build()

    @Provides
    @ApiKeyInfo
    internal fun provideApiKey(): String = BuildConfig.API_KEY

    @Provides
    @PreferenceInfo
    internal fun provideprefFileName(): String = AppConstants.PREF_NAME

    @Provides
    @Singleton
    internal fun providePrefHelper(appPreferenceHelper: AppPreferenceHelper): PreferenceHelper = appPreferenceHelper

    @Provides
    @Singleton
    internal fun provideProtectedApiHeader(@ApiKeyInfo apiKey: String, preferenceHelper: PreferenceHelper)
            : ApiHeader.ProtectedApiHeader = ApiHeader.ProtectedApiHeader(apiKey = apiKey,
            userId = preferenceHelper.getCurrentUserId(),
            accessToken = preferenceHelper.getAccessToken())

    @Provides
    @Singleton
    internal fun provideApiHelper(appApiHelper: AppApiHelper): ApiHelper = appApiHelper

    @Provides
    internal fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()

    @Provides
    @Singleton
    internal fun provideUserRepoHelper(appDatabase: AppDatabase): UserRepo = UserRepository(appDatabase.userDao())
}