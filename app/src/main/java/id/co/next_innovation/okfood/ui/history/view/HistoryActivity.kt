/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.history.view

import android.os.Bundle
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.history.interactor.HistoryMvpInteractor
import id.co.next_innovation.okfood.ui.history.presenter.HistoryMvpPresenter
import javax.inject.Inject

class HistoryActivity: BaseActivity(), HistoryView {

    @Inject
    lateinit var presenter: HistoryMvpPresenter<HistoryView, HistoryMvpInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_orders)
        presenter.onAttach(this)
    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}