/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.detail.restaurant.view

import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.next_innovation.okfood.R
import kotlinx.android.synthetic.main.adapter_dates.view.*
import java.text.Format
import java.text.SimpleDateFormat
import java.util.*

class DayAdapter(private val days: MutableList<Date>) : RecyclerView.Adapter<DayAdapter.DayViewHolder>(){

    lateinit var time: String
    override fun getItemCount() = this.days.size

    override fun onBindViewHolder(holder: DayViewHolder, position: Int) = holder.let {
        it.clear()
        it.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewRestaurant: Int) = DayViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.adapter_dates, parent, false))


    internal fun addDayToList(dates: List<Date>, time: String) {
        this.days.addAll(dates)
        this.time = time
        notifyDataSetChanged()
    }

    inner class DayViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun clear() {
        }

        fun onBind(position: Int) {
            val date: Date = days[position]
            val formatter: Format = SimpleDateFormat("EEEE")
            val hari = formatter.format(date)
            itemView.txtOperatingDay.text = hari
            itemView.txtOperatingHours.text = time

            val calendar = Calendar.getInstance()
            val dat = calendar.time
            val isToday = formatter.format(dat)
            if (itemView.txtOperatingDay.text == isToday) {
                Log.e(itemView.txtOperatingDay.text.toString(), " = "+isToday)
                itemView.txtOperatingDay.setTextColor(itemView.context.resources.getColor(R.color.black))
                itemView.txtOperatingHours.setTextColor(itemView.context.resources.getColor(R.color.black))
                itemView.txtOperatingDay.setTypeface(itemView.txtOperatingDay.typeface, Typeface.BOLD)
                itemView.txtOperatingHours.setTypeface(itemView.txtOperatingHours.typeface, Typeface.BOLD)
            } else {
                Log.e(itemView.txtOperatingDay.text.toString(), " != "+isToday)
                itemView.txtOperatingDay.setTypeface(itemView.txtOperatingDay.typeface, Typeface.NORMAL)
                itemView.txtOperatingHours.setTypeface(itemView.txtOperatingHours.typeface, Typeface.NORMAL)
            }
        }
    }

//    fun Date.toHari() {
//        val formatter: Format = SimpleDateFormat("EEEE")
//        formatter.format(toHari())
//    }
//    fun String.formatkehari(date: Date) = SimpleDateFormat("EEEE")
//    fun Date.formathari() = SimpleDateFormat("EEEE")


}
