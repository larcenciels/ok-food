package id.co.next_innovation.okfood.ui.login.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.home.view.HomeActivity
import id.co.next_innovation.okfood.ui.login.interactor.LoginMvpInteractor
import id.co.next_innovation.okfood.ui.login.presenter.LoginMvpPresenter
import id.co.next_innovation.okfood.ui.register.view.RegisterActivity
import id.co.next_innovation.okfood.util.AppConstants
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

class LoginActivity: BaseActivity(), LoginView {

    @Inject
    lateinit var presenter: LoginMvpPresenter<LoginView, LoginMvpInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
//        overridePendingTransition(R.anim.anim_pop_up, R.anim.anim_push_up)
        presenter.onAttach(this)

        linkSignUp.setOnClickListener({ gotoRegisterActivity() })
        btnSignIn.setOnClickListener({ presenter.signIn(txtEmail.text.toString(), txtPassword.text.toString()) })
    }

    override fun gotoHomeActivity() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun gotoRegisterActivity() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }

    override fun showValidationMessage(errorCode: Int) {
        when (errorCode) {
            AppConstants.EMPTY_EMAIL_ERROR -> Toast.makeText(this, getString(R.string.empty_email_error_message), Toast.LENGTH_LONG).show()
            AppConstants.INVALID_EMAIL_ERROR -> Toast.makeText(this, getString(R.string.invalid_email_error_message), Toast.LENGTH_LONG).show()
            AppConstants.EMPTY_PASSWORD_ERROR -> Toast.makeText(this, getString(R.string.empty_password_error_message), Toast.LENGTH_LONG).show()
            AppConstants.LOGIN_FAILURE -> Toast.makeText(this, getString(R.string.login_failure), Toast.LENGTH_LONG).show()
        }
    }

    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }
}