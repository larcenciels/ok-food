package id.co.next_innovation.okfood.ui.helper

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class GridSpacingItemDecorator(spancount: Int, spacing: Int, includeEdge: Boolean) : RecyclerView.ItemDecoration() {

    var mSpanCount: Int = spancount
    var mSpacing: Int = spacing
    var mIncludeEdge: Boolean = includeEdge

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {
        val position: Int = parent!!.getChildAdapterPosition(view)
        var column: Int = position % mSpanCount

        if (mIncludeEdge) {
            outRect?.left = mSpacing - column * mSpacing / mSpanCount
            outRect?.right = (column + 1) * mSpacing / mSpanCount

            if (position < mSpanCount) {
                outRect?.top = mSpacing
            }
            outRect?.bottom = mSpacing
        } else {
            outRect?.left = column * mSpacing / mSpanCount; // column * ((1f / spanCount) * spacing)
            outRect?.right = mSpanCount - (column + 1) * mSpacing / mSpanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= mSpanCount) {
                outRect?.top = mSpacing; // item top
            }
        }
    }
}