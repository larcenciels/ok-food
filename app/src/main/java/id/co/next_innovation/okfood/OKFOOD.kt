package id.co.next_innovation.okfood

import android.app.Activity
import android.app.Application
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import id.co.next_innovation.okfood.di.component.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import javax.inject.Inject

/**
 * Created by amitshekhar on 24/12/17.
 */
class OKFOOD : Application(), HasActivityInjector {

    @Inject
    lateinit internal var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector() = activityDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this)

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("font/neosans_pro_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build())

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY)
        }
    }


}