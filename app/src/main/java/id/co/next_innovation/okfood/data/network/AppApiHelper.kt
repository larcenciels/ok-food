package id.co.next_innovation.okfood.data.network

import com.rx2androidnetworking.Rx2AndroidNetworking
import id.co.next_innovation.okfood.data.network.reponse.*
import id.co.next_innovation.okfood.data.network.request.UserRequest
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by jyotidubey on 04/01/18.
 */
class AppApiHelper @Inject constructor(private val apiHeader: ApiHeader) : ApiHelper {

    override fun performSignIn(request: UserRequest.Login): Observable<UserResponse.Login> =
            Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_LOGIN)
                    .addBodyParameter(request)
                    .build()
                    .getObjectObservable(UserResponse.Login::class.java)

    override fun getGroupMenus(): Observable<GroupMenuResponse> =
            Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GROUP_MENU)
                    .build()
                    .getObjectObservable(GroupMenuResponse::class.java)

    override fun getTypes(): Observable<TypeResponse> =
            Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_TYPE)
                    .build()
                    .getObjectObservable(TypeResponse::class.java)

    override fun getRestaurants(): Observable<RestaurantResponse> =
            Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_RESTAURANT)
                    .build()
                    .getObjectObservable(RestaurantResponse::class.java)

    override fun getMenuCategoriesByRestaurantId(restaurantId: Int): Observable<CategoryResponse> =
            Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_CATEGORY)
                    .addPathParameter(restaurantId)
                    .build()
                    .getObjectObservable(CategoryResponse::class.java)

    override fun getMenuByCategoryId(categoryId: Int): Observable<MenuResponse> =
            Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_MENU)
                    .addPathParameter(categoryId)
                    .build()
                    .getObjectObservable(MenuResponse::class.java)

}