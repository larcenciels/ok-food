/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.detail.restaurant.interactor

import android.content.Context
import id.co.next_innovation.okfood.data.network.ApiHelper
import id.co.next_innovation.okfood.data.preferences.PreferenceHelper
import id.co.next_innovation.okfood.ui.base.interactor.BaseInteractor
import id.co.next_innovation.okfood.ui.detail.restaurant.view.DetailRestaurantView
import javax.inject.Inject

class DetailRestaurantInteractor@Inject constructor(private val mContext: Context, preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper),
       DetailRestaurantMvpInteractor {

}