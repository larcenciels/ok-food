/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.search.presenter

import id.co.next_innovation.okfood.ui.base.presenter.MVPPresenter
import id.co.next_innovation.okfood.ui.search.interactor.SearchMvpInteractor
import id.co.next_innovation.okfood.ui.search.view.SearchView

interface SearchMvpPresenter<V: SearchView, I: SearchMvpInteractor> : MVPPresenter<V, I>