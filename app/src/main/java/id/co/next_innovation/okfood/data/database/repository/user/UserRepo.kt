/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.database.repository.user

import io.reactivex.Observable

interface UserRepo {

    fun insertUser(user: User): Observable<Boolean>

    fun loadUserById(id: Int): Observable<User>
}