/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu.view

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.util.Log
import android.view.MenuItem
import com.google.android.gms.maps.model.LatLng
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.data.database.model.Category
import id.co.next_innovation.okfood.data.database.model.Restaurant
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.detail.restaurant.view.DetailRestaurantActivity
import id.co.next_innovation.okfood.ui.menu.item.view.ItemFragment
import id.co.next_innovation.okfood.ui.menu.interactor.MenuMvpInteractor
import id.co.next_innovation.okfood.ui.menu.presenter.MenuMvpPresenter
import id.co.next_innovation.okfood.util.CommonUtil
import id.co.next_innovation.okfood.util.extension.loadImage
import kotlinx.android.synthetic.main.activity_list_menu.*
import kotlinx.android.synthetic.main.food_merchant_info.*
import kotlinx.android.synthetic.main.food_merchant_menu_image_new.*
import java.util.*
import javax.inject.Inject

class MenuActivity : BaseActivity(), MenuView, HasSupportFragmentInjector {

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var presenter: MenuMvpPresenter<MenuView, MenuMvpInteractor>
    private lateinit var menuPagerAdapter: MenuPagerAdapter
    private lateinit var mCategories: List<Category>
    private lateinit var restaurant: Restaurant

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_menu)
        presenter.onAttach(this)

        menuPagerAdapter = MenuPagerAdapter(supportFragmentManager)
        setup()

        layout_three_dots.setOnClickListener {

            val intent = Intent(this, DetailRestaurantActivity::class.java)
            intent.putExtra("restaurant", restaurant)
            startActivity(intent)
        } 
    }

    fun setup() {

        restaurant = intent.getSerializableExtra("restaurant") as Restaurant
        
        presenter.getCategoriesMenuRestaurant(restaurant.id)

        setupTabs()


        menu_app_bar_layout.addOnOffsetChangedListener({
            appBarLayout, verticalOffset ->
                var isShow = true
                var scrollRange: Int = -1
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    toolbar.title = restaurant.name
                    collapsing_toolbar.title = restaurant.name
                    toolbar.setBackgroundColor(resources.getColor(android.R.color.white))
                    toolbar.setTitleTextColor(resources.getColor(android.R.color.black))

                    setSupportActionBar(toolbar)
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    supportActionBar?.setDisplayShowHomeEnabled(true)

                } else if (isShow) {
                    toolbar.title = " "
                    collapsing_toolbar.title = " "
                    toolbar.setBackgroundColor(resources.getColor(android.R.color.transparent))
                    setSupportActionBar(toolbar)
                    supportActionBar?.setDisplayHomeAsUpEnabled(true)
                    isShow = false
                }
        })

        toolbar.setNavigationOnClickListener { onBackPressed() }

        img_merchant.loadImage(restaurant.image)
        text_merchant_name.text = restaurant.name
        text_merchant_business_hour.text = "Open "+restaurant.open +" - "+ restaurant.close
        val pos1 = LatLng(presenter.getLat(), presenter.getLon())
        val pos2 = LatLng(restaurant.latitude, restaurant.longitude)
        val dist: Double = CommonUtil.distance(pos1, pos2)
        text_merchant_address.text = dist.toString() +" km - "+ restaurant.street + ", "+restaurant.district + ", "+restaurant.city
        val currentTime = Calendar.getInstance()

        if (isTimeBetweenTwoHours(restaurant.open.replace(":", "").toInt()/10000, restaurant.close.replace(":", "").toInt()/10000, currentTime)) {
            text_merchant_status.text = resources.getString(R.string.open)
            text_merchant_status.setTextColor(resources.getColor(R.color.color_open))
            layout_opened_closed.background = resources.getDrawable(R.drawable.food_button_stroke_green)
        } else if (!isTimeBetweenTwoHours(restaurant.open.replace(":", "").toInt()/10000, restaurant.close.replace(":", "").toInt()/10000, currentTime)) {
            text_merchant_status.text = resources.getString(R.string.close)
            text_merchant_status.setTextColor(resources.getColor(R.color.color_closed))
            layout_opened_closed.background = resources.getDrawable(R.drawable.food_button_stroke_red)
        }


    }

    fun setupTabs() {
        menuPagerAdapter = MenuPagerAdapter(supportFragmentManager)
        pager_merchant_menu_section.adapter = menuPagerAdapter
        menuPagerAdapter.notifyDataSetChanged()
        tabs_merchant_menu.setupWithViewPager(pager_merchant_menu_section)
    }

    override fun showCategoriesOnTabs(categories: List<Category>) {
        categories.let {
            mCategories = categories
        }
    }

    override fun addTab(category: Category) {
        menuPagerAdapter.addFragment(ItemFragment.newInstance(category), category.name)
        menuPagerAdapter.notifyDataSetChanged()
    }

    override fun offScreenPageLimit(size: Int) {
        tabs_merchant_menu.setupWithViewPager(pager_merchant_menu_section)
        menuPagerAdapter.notifyDataSetChanged()

    }

    override fun clearTabs() {
        menuPagerAdapter.clearItems()
        menuPagerAdapter.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home ->  onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * @return true if Current Time is between fromHour and toHour
     */
    fun isTimeBetweenTwoHours(fromHour: Int, toHour: Int, now: Calendar): Boolean {
        //Start Time
        val from = Calendar.getInstance()
        from.set(Calendar.HOUR_OF_DAY, fromHour)
        from.set(Calendar.MINUTE, 0)
        //Stop Time
        val to = Calendar.getInstance()
        to.set(Calendar.HOUR_OF_DAY, toHour)
        to.set(Calendar.MINUTE, 0)

        if (to.before(from)) {
            if (now.after(to))
                to.add(Calendar.DATE, 1)
            else
                from.add(Calendar.DATE, -1)
        }
        return now.after(from) && now.before(to)
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }


    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {
    }
}