/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.network.reponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class TypeResponse internal constructor(
        @SerializedName("error")
        @Expose
        var error: Boolean ,
        @SerializedName("status")
        @Expose
        var status: String ,
        @SerializedName("status_code")
        @Expose
        var statusCode: Int ,
        @SerializedName("message")
        @Expose
        var message: String ,
        @SerializedName("data")
        @Expose
        var data: DataType
)