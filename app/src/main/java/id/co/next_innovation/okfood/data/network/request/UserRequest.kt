/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.network.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserRequest {

    data class Login internal constructor(
            @Expose
            @SerializedName("email")
            internal val email: String,
            @Expose
            @SerializedName("password")
            internal val password: String)

    data class Register internal constructor(
            @Expose
            @SerializedName("name")
            internal val name: String,
            @Expose
            @SerializedName("email")
            internal val email: String,
            @Expose
            @SerializedName("phone")
            internal val phone: String,
            @Expose
            @SerializedName("password")
            internal val password: String
    )
}