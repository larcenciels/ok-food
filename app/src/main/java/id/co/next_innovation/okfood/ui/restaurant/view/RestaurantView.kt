/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.restaurant.view

import id.co.next_innovation.okfood.data.database.model.Restaurant
import id.co.next_innovation.okfood.ui.base.view.MVPView

interface RestaurantView : MVPView {

    fun onViewPrepared()
    fun showRestaurants(restaurants: List<Restaurant>?)
}