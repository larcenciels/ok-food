/*
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.helper.slider;

import java.util.Arrays;
import java.util.List;

public class DemoBanner {

    public static DemoBanner get() {
        return new DemoBanner();
    }

    public List<Banner> getBanner() {
        return Arrays.asList(
                new Banner(1, "", "https://lelogama.go-jek.com/post_featured_image/blog_mujigae.jpg"),
                new Banner(2, "", "https://lelogama.go-jek.com/post_featured_image/blog_AW.jpg")
        );
    }
}
