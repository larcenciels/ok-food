package id.co.next_innovation.okfood.ui.base.interactor

import id.co.next_innovation.okfood.data.network.ApiHelper
import id.co.next_innovation.okfood.data.preferences.PreferenceHelper
import id.co.next_innovation.okfood.util.AppConstants

/**
 * Created by jyotidubey on 04/01/18.
 */
open class BaseInteractor() : MVPInteractor {

    protected lateinit var preferenceHelper: PreferenceHelper
    protected lateinit var apiHelper: ApiHelper

    constructor(preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : this() {
        this.preferenceHelper = preferenceHelper
        this.apiHelper = apiHelper
    }

    override fun isUserLoggedIn(): Boolean {
        return preferenceHelper.isLoggedIn()
    }

    override fun performUserLogout() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    //    override fun performUserLogout() = preferenceHelper.let {
//        it.setCurrentUserId(null)
//        it.setAccessToken(null)
//        it.setCurrentUserEmail(null)
//        it.setCurrentUserLoggedInMode(AppConstants.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT)
//    }

}