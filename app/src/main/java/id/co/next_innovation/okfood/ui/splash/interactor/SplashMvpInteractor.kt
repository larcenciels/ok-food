package id.co.next_innovation.okfood.ui.splash.interactor

import id.co.next_innovation.okfood.ui.base.interactor.MVPInteractor

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

interface SplashMvpInteractor : MVPInteractor {

}