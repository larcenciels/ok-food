/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.home

import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.home.interactor.HomeInteractor
import id.co.next_innovation.okfood.ui.home.interactor.HomeMvpInteractor
import id.co.next_innovation.okfood.ui.home.presenter.HomeMvpPresenter
import id.co.next_innovation.okfood.ui.home.presenter.HomePresenter
import id.co.next_innovation.okfood.ui.home.view.HomeView
import id.co.next_innovation.okfood.ui.home.view.adapter.GroupMenuAdapter
import id.co.next_innovation.okfood.ui.home.view.adapter.TypeAdapter

@Module
class HomeActivityModule {

    @Provides
    internal fun provideHomeInteractor(homeInteractor: HomeInteractor): HomeMvpInteractor = homeInteractor

    @Provides
    internal fun provideHomePresenter(homePresenter: HomePresenter<HomeView, HomeMvpInteractor>):
            HomeMvpPresenter<HomeView, HomeMvpInteractor> = homePresenter

    @Provides
    internal fun provideGroupMenuAdapter(): GroupMenuAdapter = GroupMenuAdapter(ArrayList())

    @Provides
    internal fun provideTypeMenuAdapter(): TypeAdapter = TypeAdapter(ArrayList())
}