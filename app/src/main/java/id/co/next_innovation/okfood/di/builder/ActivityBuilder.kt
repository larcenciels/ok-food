package id.co.next_innovation.okfood.di.builder

import id.co.next_innovation.okfood.ui.splash.SplashActivityModule
import id.co.next_innovation.okfood.ui.splash.view.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.co.next_innovation.okfood.ui.account.AccountActivityModule
import id.co.next_innovation.okfood.ui.account.view.AccountActivity
import id.co.next_innovation.okfood.ui.detail.restaurant.DetailRestaurantActivityModule
import id.co.next_innovation.okfood.ui.detail.restaurant.view.DetailRestaurantActivity
import id.co.next_innovation.okfood.ui.history.HistoryActivityModule
import id.co.next_innovation.okfood.ui.history.view.HistoryActivity
import id.co.next_innovation.okfood.ui.home.HomeActivityModule
import id.co.next_innovation.okfood.ui.home.view.HomeActivity
import id.co.next_innovation.okfood.ui.login.LoginActivityModule
import id.co.next_innovation.okfood.ui.login.view.LoginActivity
import id.co.next_innovation.okfood.ui.menu.MenuActivityModule
import id.co.next_innovation.okfood.ui.menu.item.ItemFragmentProvider
import id.co.next_innovation.okfood.ui.menu.item.view.ItemFragment
import id.co.next_innovation.okfood.ui.menu.view.MenuActivity
import id.co.next_innovation.okfood.ui.register.RegisterActivityModule
import id.co.next_innovation.okfood.ui.register.view.RegisterActivity
import id.co.next_innovation.okfood.ui.restaurant.RestaurantActivityModule
import id.co.next_innovation.okfood.ui.restaurant.view.RestaurantActivity
import id.co.next_innovation.okfood.ui.search.SearchActivityModule
import id.co.next_innovation.okfood.ui.search.view.SearchActivity
import id.co.next_innovation.okfood.ui.social.SocialActivityModule
import id.co.next_innovation.okfood.ui.social.view.SocialActivity

/**
 * Created by jyotidubey on 05/01/18.
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(SplashActivityModule::class)])
    abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [(LoginActivityModule::class)])
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [(RegisterActivityModule::class)])
    abstract fun bindRegisterActivity(): RegisterActivity

    @ContributesAndroidInjector(modules = [(HomeActivityModule::class)])
    abstract fun bindHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [(RestaurantActivityModule::class)])
    abstract fun bindRestaurantActivity(): RestaurantActivity

    @ContributesAndroidInjector(modules = [(SearchActivityModule::class)])
    abstract fun bindSearchActivity(): SearchActivity

    @ContributesAndroidInjector(modules = [(MenuActivityModule::class), (ItemFragmentProvider::class)])
    abstract fun bindMenuActivity(): MenuActivity

    @ContributesAndroidInjector(modules = [(SocialActivityModule::class)])
    abstract fun bindSocialActivity(): SocialActivity

    @ContributesAndroidInjector(modules = [(HistoryActivityModule::class)])
    abstract fun bindHistoryActivity(): HistoryActivity

    @ContributesAndroidInjector(modules = [(AccountActivityModule::class)])
    abstract fun bindAccountActivity(): AccountActivity

    @ContributesAndroidInjector(modules = [(DetailRestaurantActivityModule::class)])
    abstract fun bindDetailRestaurantActivity(): DetailRestaurantActivity

}