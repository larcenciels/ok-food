package id.co.next_innovation.okfood.data.preferences

import id.co.next_innovation.okfood.util.AppConstants

/**
 * Created by jyotidubey on 04/01/18.
 */
interface PreferenceHelper {

    fun getCurrentUserId(): Long?

    fun setCurrentUserId(userId: Long?)

    fun getCurrentUserName(): String

    fun setCurrentUserName(userName: String?)

    fun getCurrentUserEmail(): String?

    fun setCurrentUserEmail(email: String?)

    fun getAccessToken(): String?

    fun setAccessToken(accessToken: String?)

    fun setLoggedIn(loggedIn: Boolean)
    fun isLoggedIn(): Boolean
    fun setTmpLat(lat: Double?)
    fun getTmpLat(): Double
    fun setTmpLon(lon: Double?)
    fun getTmpLon(): Double

}