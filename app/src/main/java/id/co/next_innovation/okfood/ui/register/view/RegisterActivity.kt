/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.register.view

import android.os.Bundle
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.register.interactor.RegisterMvpInteractor
import id.co.next_innovation.okfood.ui.register.presenter.RegisterMvpPresenter
import javax.inject.Inject

class RegisterActivity: BaseActivity(), RegisterView {

    @Inject
    lateinit var presenter: RegisterMvpPresenter<RegisterView, RegisterMvpInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        presenter.onAttach(this)

    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {
    }
}