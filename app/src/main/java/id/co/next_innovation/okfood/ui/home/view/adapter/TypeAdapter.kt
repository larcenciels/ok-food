/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.home.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.data.database.model.Type
import id.co.next_innovation.okfood.util.extension.loadImage
import kotlinx.android.synthetic.main.adapter_type.view.*

class TypeAdapter(private val typeList: MutableList<Type>) : RecyclerView.Adapter<TypeAdapter.TypeViewHolder>() {

    override fun getItemCount() = this.typeList.size

    override fun onBindViewHolder(holder: TypeViewHolder, position: Int) = holder.let {
        it.clear()
        it.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = TypeViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.adapter_type, parent, false))


    internal fun addTypeToList(types: List<Type>) {
        this.typeList.addAll(types)
        notifyDataSetChanged()
    }

    inner class TypeViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun clear() {
            itemView.image.setImageDrawable(null)
            itemView.name.text = ""
        }

        fun onBind(position: Int) {

            val (id, name, image) = typeList[position]

            inflateData(id, name, image)
            //setItemClickListener(blogUrl)
        }

        private fun inflateData(id: Int?, name: String?, image: String?) {
            name?.let { itemView.name.text = it }
            image?.let {
                itemView.image.loadImage(it)
            }
        }
    }


}