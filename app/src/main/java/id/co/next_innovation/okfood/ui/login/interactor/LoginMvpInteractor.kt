package id.co.next_innovation.okfood.ui.login.interactor

import id.co.next_innovation.okfood.data.database.repository.user.User
import id.co.next_innovation.okfood.data.network.reponse.UserResponse
import id.co.next_innovation.okfood.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

interface LoginMvpInteractor: MVPInteractor {

    fun performSignIn(email: String, password: String) : Observable<UserResponse.Login>
    fun updateUserLoggedIn(user: User)
    fun isLoggedIn(): Boolean? = null
}