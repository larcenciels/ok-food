package id.co.next_innovation.okfood.util

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.location.Location
import androidx.core.graphics.drawable.toDrawable
import com.google.android.gms.maps.model.LatLng
import id.co.next_innovation.okfood.R
import java.math.BigDecimal
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by jyotidubey on 11/01/18.
 */
object CommonUtil {

    fun showLoadingDialog(context: Context?): ProgressDialog {
        val progressDialog = ProgressDialog(context)
        progressDialog.let {
            it.show()
            it.window?.setBackgroundDrawable(Color.TRANSPARENT.toDrawable())
            it.setContentView(R.layout.progress_dialog)
            it.isIndeterminate = true
            it.setCancelable(false)
            it.setCanceledOnTouchOutside(false)
            return it
        }
    }

    fun isEmailValid(email: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        pattern = Pattern.compile(EMAIL_PATTERN)
        matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun calculateDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val tetha: Double = lon1 - lon2
        var dist: Double = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) * Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(tetha))

        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist = dist * 60 * 1.1515

        return dist
    }

    fun distance(pos1: LatLng, pos2: LatLng): Double {
        val loc1 = Location("")
        loc1.latitude = pos1.latitude
        loc1.longitude = pos1.longitude

        val loc2 = Location("")
        loc2.latitude = pos2.latitude
        loc2.longitude = pos2.longitude

        val distanceInKm: Double = loc1.distanceTo(loc2) * 0.001
        return distanceInKm.roundTo2Decimal()
    }

    fun Double.roundTo2Decimal() = BigDecimal(this).setScale(2, BigDecimal.ROUND_HALF_UP).toDouble()

    private fun deg2rad(deg: Double): Double {
        return (deg * Math.PI / 180.0)
    }

    private fun rad2deg(rad: Double): Double {
        return (rad * 180.0 / Math.PI);
    }

}