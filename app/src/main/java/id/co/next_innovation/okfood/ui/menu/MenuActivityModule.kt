/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu

import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.menu.interactor.MenuInteractor
import id.co.next_innovation.okfood.ui.menu.interactor.MenuMvpInteractor
import id.co.next_innovation.okfood.ui.menu.presenter.MenuMvpPresenter
import id.co.next_innovation.okfood.ui.menu.presenter.MenuPresenter
import id.co.next_innovation.okfood.ui.menu.view.MenuView

@Module
class MenuActivityModule {

    @Provides
    internal fun provideMenuInteractor(menuInteractor: MenuInteractor): MenuMvpInteractor = menuInteractor

    @Provides
    internal fun provideMenuPresenter(menuPresenter: MenuPresenter<MenuView, MenuMvpInteractor>):
            MenuMvpPresenter<MenuView, MenuMvpInteractor> = menuPresenter
}