package id.co.next_innovation.okfood.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import id.co.next_innovation.okfood.data.database.repository.user.User
import id.co.next_innovation.okfood.data.database.repository.user.UserDao

/**
 * Created by jyotidubey on 03/01/18.
 */
@Database(entities = [(User::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}