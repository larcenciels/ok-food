package id.co.next_innovation.okfood.ui.base.presenter

import id.co.next_innovation.okfood.ui.base.interactor.MVPInteractor
import id.co.next_innovation.okfood.ui.base.view.MVPView

/**
 * Created by jyotidubey on 04/01/18.
 */
interface MVPPresenter<V : MVPView, I : MVPInteractor> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

}