/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.social

import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.social.interactor.SocialInteractor
import id.co.next_innovation.okfood.ui.social.interactor.SocialMvpInteractor
import id.co.next_innovation.okfood.ui.social.presenter.SocialMvpPresenter
import id.co.next_innovation.okfood.ui.social.presenter.SocialPresenter
import id.co.next_innovation.okfood.ui.social.view.SocialView

@Module
class SocialActivityModule {

    @Provides
    internal fun provideSocialInteractor(socialInteractor: SocialInteractor): SocialMvpInteractor = socialInteractor

    @Provides
    internal fun provideSocialPresenter(socialPresenter: SocialPresenter<SocialView, SocialMvpInteractor>):
            SocialMvpPresenter<SocialView, SocialMvpInteractor> = socialPresenter
}