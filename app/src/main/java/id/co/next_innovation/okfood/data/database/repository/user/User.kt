/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.database.repository.user

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "users")
data class User(
        @PrimaryKey
        @Expose
        @SerializedName("id")
        var id: Int,
        @Expose
        @SerializedName("name")
        var name: String,
        @Expose
        @SerializedName("email")
        var email: String,
        @Expose
        @SerializedName("created_at")
        var created_at: String,
        @Expose
        @SerializedName("api_token")
        var api_token: String,
        @Expose
        @SerializedName("phone")
        var phone: String
)