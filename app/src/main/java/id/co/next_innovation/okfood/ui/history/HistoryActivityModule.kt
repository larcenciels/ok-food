/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.history

import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.history.interactor.HistoryInteractor
import id.co.next_innovation.okfood.ui.history.interactor.HistoryMvpInteractor
import id.co.next_innovation.okfood.ui.history.presenter.HistoryMvpPresenter
import id.co.next_innovation.okfood.ui.history.presenter.HistoryPresenter
import id.co.next_innovation.okfood.ui.history.view.HistoryView

@Module
class HistoryActivityModule {

    @Provides
    internal fun provideHistoryInteractor(historyInteractor: HistoryInteractor): HistoryMvpInteractor = historyInteractor

    @Provides
    internal fun provideHistoryPresenter(historyPresenter: HistoryPresenter<HistoryView, HistoryMvpInteractor>):
            HistoryMvpPresenter<HistoryView, HistoryMvpInteractor> = historyPresenter
}