/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu.item.view

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import id.co.next_innovation.okfood.R
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import id.co.next_innovation.okfood.data.database.model.Category
import id.co.next_innovation.okfood.data.database.model.Menu
import id.co.next_innovation.okfood.ui.base.view.BaseFragment
import id.co.next_innovation.okfood.ui.menu.item.interactor.ItemMvpInteractor
import id.co.next_innovation.okfood.ui.menu.item.presenter.ItemMvpPresenter
import kotlinx.android.synthetic.main.fragment_menu.*
import javax.inject.Inject

class ItemFragment : BaseFragment(), ItemView {

    companion object {
        fun newInstance(category: Category): ItemFragment {
            val args =  Bundle()
            args.putSerializable("category", category)
            val menuFragment =  ItemFragment()
            menuFragment.arguments = args
            return menuFragment
        }
    }

    @Inject
    internal lateinit var itemAdapter: ItemAdapter

    @Inject
    internal lateinit var layoutManager: LinearLayoutManager

    @Inject
    internal lateinit var presenter: ItemMvpPresenter<ItemView, ItemMvpInteractor>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_menu, container, false)



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        presenter.onAttach(this)
        super.onViewCreated(view, savedInstanceState)
    }


    override fun setUp() {

        val bundle: Bundle = arguments ?: return
        val category: Category = bundle.getSerializable("category") as Category
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        menu_recycler_view.layoutManager = layoutManager
        menu_recycler_view.itemAnimator = DefaultItemAnimator()
        menu_recycler_view.adapter = itemAdapter
        presenter.onViewPrepared(category.id)
    }

    override fun showMenus(menus: List<Menu>) {
        menus.let {
            itemAdapter.addMenuToList(it)
        }
    }
}