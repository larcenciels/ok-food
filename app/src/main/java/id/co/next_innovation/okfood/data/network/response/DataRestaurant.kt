/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.network.reponse

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import id.co.next_innovation.okfood.data.database.model.Restaurant

class DataRestaurant internal constructor(
        @SerializedName("restaurants")
        @Expose
        var restaurant: List<Restaurant>
)