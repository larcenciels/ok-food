/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.search.view

import android.os.Bundle
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.search.interactor.SearchMvpInteractor
import id.co.next_innovation.okfood.ui.search.presenter.SearchMvpPresenter
import kotlinx.android.synthetic.main.search_toolbar.*
import javax.inject.Inject

class SearchActivity : BaseActivity(), SearchView {

    @Inject
    lateinit var presenter: SearchMvpPresenter<SearchView, SearchMvpInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        presenter.onAttach(this)

        img_back.setOnClickListener({finish()})
        img_clear_search.setOnClickListener({input_search.text.clear()})
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {
    }
}