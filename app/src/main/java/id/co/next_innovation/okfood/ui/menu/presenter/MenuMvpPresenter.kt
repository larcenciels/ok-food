/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu.presenter

import id.co.next_innovation.okfood.ui.base.presenter.MVPPresenter
import id.co.next_innovation.okfood.ui.menu.interactor.MenuMvpInteractor
import id.co.next_innovation.okfood.ui.menu.view.MenuView

interface MenuMvpPresenter<V: MenuView, I: MenuMvpInteractor> : MVPPresenter<V, I> {

    fun getLat(): Double
    fun getLon(): Double

    fun getCategoriesMenuRestaurant(restaurantId: Int)
}