/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.restaurant

import android.support.v7.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.restaurant.interactor.RestaurantInteractor
import id.co.next_innovation.okfood.ui.restaurant.interactor.RestaurantMvpInteractor
import id.co.next_innovation.okfood.ui.restaurant.presenter.RestaurantMvpPresenter
import id.co.next_innovation.okfood.ui.restaurant.presenter.RestaurantPresenter
import id.co.next_innovation.okfood.ui.restaurant.view.RestaurantActivity
import id.co.next_innovation.okfood.ui.restaurant.view.RestaurantAdapter
import id.co.next_innovation.okfood.ui.restaurant.view.RestaurantView

@Module
class RestaurantActivityModule {

    @Provides
    internal fun provideRestaurantInteractor(restaurantInteractor: RestaurantInteractor): RestaurantMvpInteractor = restaurantInteractor

    @Provides
    internal fun provideRestaurantPresenter(restaurantPresenter: RestaurantPresenter<RestaurantView, RestaurantMvpInteractor>)
                :RestaurantMvpPresenter<RestaurantView, RestaurantMvpInteractor> =  restaurantPresenter

    @Provides
    internal fun provideRestaurantAdapter(): RestaurantAdapter = RestaurantAdapter(ArrayList())

    @Provides
    internal fun provideLinearLayoutManager(activity: RestaurantActivity): LinearLayoutManager = LinearLayoutManager(activity)
}