/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.register

import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.register.interactor.RegisterInteractor
import id.co.next_innovation.okfood.ui.register.interactor.RegisterMvpInteractor
import id.co.next_innovation.okfood.ui.register.presenter.RegisterMvpPresenter
import id.co.next_innovation.okfood.ui.register.presenter.RegisterPresenter
import id.co.next_innovation.okfood.ui.register.view.RegisterView
import javax.inject.Inject

@Module
class RegisterActivityModule {

    @Provides
    internal fun provideRegisterInteractor(regiterInteractor: RegisterInteractor): RegisterMvpInteractor = regiterInteractor

    @Provides
    internal fun provideRegisterPreenter(registerPresenter: RegisterPresenter<RegisterView, RegisterMvpInteractor>):
            RegisterMvpPresenter<RegisterView, RegisterMvpInteractor> = registerPresenter
}