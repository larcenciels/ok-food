/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.register.presenter

import id.co.next_innovation.okfood.ui.base.presenter.MVPPresenter
import id.co.next_innovation.okfood.ui.register.interactor.RegisterMvpInteractor
import id.co.next_innovation.okfood.ui.register.view.RegisterView

interface RegisterMvpPresenter<V: RegisterView, I: RegisterMvpInteractor>: MVPPresenter<V, I>