package id.co.next_innovation.okfood.ui.login.presenter

import id.co.next_innovation.okfood.ui.base.presenter.BasePresenter
import id.co.next_innovation.okfood.ui.login.interactor.LoginMvpInteractor
import id.co.next_innovation.okfood.ui.login.view.LoginView
import id.co.next_innovation.okfood.util.AppConstants
import id.co.next_innovation.okfood.util.CommonUtil
import id.co.next_innovation.okfood.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

class LoginPresenter<V: LoginView, I: LoginMvpInteractor>
    @Inject
    internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) :
        BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable),
        LoginMvpPresenter<V, I> {

    override fun signIn(email: String, password: String) {
        when {
            email.isEmpty() -> getView()?.showValidationMessage(AppConstants.EMPTY_EMAIL_ERROR)
            !CommonUtil.isEmailValid(email) -> getView()?.showValidationMessage(AppConstants.INVALID_EMAIL_ERROR)
            password.isEmpty() -> getView()?.showValidationMessage(AppConstants.EMPTY_PASSWORD_ERROR)
            else -> {
                getView()?.showProgress()
                interactor?.let {
                    compositeDisposable.add(it.performSignIn(email, password)
                            .compose(schedulerProvider.ioToMainObservableScheduler())
                            .subscribe({ loginResponse ->
                                getView()?.hideProgress()
                                if (!loginResponse.error) {
                                    getView()?.let {
                                        it.hideProgress()
                                        it.gotoHomeActivity()
                                        interactor?.updateUserLoggedIn(loginResponse.user)
                                    }

                                } else {
                                    getView()?.let {
                                        it.hideProgress()
                                        it.showValidationMessage(AppConstants.ERROR_LOGIN)
                                    }
                                }
                            }, { err -> Timber.e(err) }))
                }
            }
        }
    }
}
