package id.co.next_innovation.okfood.data.network

import id.co.next_innovation.okfood.BuildConfig

/**
 * Created by jyotidubey on 11/01/18.
 */
object ApiEndPoint {

    val ENDPOINT_GOOGLE_LOGIN = BuildConfig.BASE_URL + "/588d14f4100000a9072d2943"
    val ENDPOINT_FACEBOOK_LOGIN = BuildConfig.BASE_URL + "/588d15d3100000ae072d2944"
    val ENDPOINT_SERVER_LOGIN = BuildConfig.BASE_URL + "/588d15f5100000a8072d2945"
    val ENDPOINT_LOGOUT = BuildConfig.BASE_URL + "/588d161c100000a9072d2946"
    val ENDPOINT_BLOG = BuildConfig.BASE_URL + "/5926ce9d11000096006ccb30"
    val ENDPOINT_OPEN_SOURCE = BuildConfig.BASE_URL + "/5926c34212000035026871cd"

    val ENDPOINT_LOGIN = BuildConfig.BASE_URL + "/api/v1/login"
    val ENDPOINT_REGISTER = BuildConfig.BASE_URL + "/api/v1/register"
    val ENDPOINT_GROUP_MENU = "https://next-innovation.co.id/test.json"
    val ENDPOINT_TYPE = "https://next-innovation.co.id/tes.json"
    val ENDPOINT_RESTAURANT = "https://next-innovation.co.id/tess.json"
    val ENDPOINT_CATEGORY = "https://next-innovation.co.id/tescat.json"
    val ENDPOINT_MENU = "https://next-innovation.co.id/tesmenu.json"
}