package id.co.next_innovation.okfood.ui.splash.presenter

import id.co.next_innovation.okfood.ui.base.presenter.BasePresenter
import id.co.next_innovation.okfood.ui.splash.interactor.SplashMvpInteractor
import id.co.next_innovation.okfood.ui.splash.view.SplashView
import id.co.next_innovation.okfood.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

class SplashPresenter<V : SplashView, I : SplashMvpInteractor>
    @Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) :
        BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable),
        SplashMvpPresenter<V, I> {

    override fun onAttach(view: V?) {
        super.onAttach(view)
    }

    override fun decideActivityToGo() = getView()?.let {
        if (isUserLoggedIn())
            it.gotoHomeActivity()
        else
            it.gotoLoginActivity()
    }

    private fun isUserLoggedIn() : Boolean {
        interactor?.let { return it.isUserLoggedIn() }
        return false
    }
}