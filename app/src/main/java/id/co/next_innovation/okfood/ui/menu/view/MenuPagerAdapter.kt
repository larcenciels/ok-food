/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu.view

import android.content.Context
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View

class MenuPagerAdapter(fragmentManager: FragmentManager)
    : FragmentStatePagerAdapter(fragmentManager) {

    var fragments: ArrayList<Fragment> = ArrayList()
    var fragmentsTitle: ArrayList<String> = ArrayList()

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    fun addFragment(fragment: Fragment, title: String) {
        fragments.add(fragment)
        fragmentsTitle.add(title)
    }

    fun addFragments(mFragments: ArrayList<Fragment>, titles: List<String>) {
        mFragments.clear()
        fragmentsTitle.clear()
        fragments.addAll(mFragments)
        fragmentsTitle.addAll(titles)
        notifyDataSetChanged()
    }

    fun removeFrag(position: Int) {
       // removeTab[position]
        val fragment: Fragment = fragments[position]
        fragments.remove(fragment)
        fragmentsTitle.removeAt(position)
        //destroyFragmentView(fragment)
        notifyDataSetChanged()
    }

    override fun getItemPosition(`object`: Any): Int {
        if (fragments.contains(`object`)) {
            return fragments.indexOf(`object`)
        } else {
            return PagerAdapter.POSITION_NONE
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentsTitle[position]
    }

    override fun saveState(): Parcelable? {
        return null
    }

    fun clearItems() {
        fragments.clear()
        fragmentsTitle.clear()
        notifyDataSetChanged()
    }
}