/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.database.repository.user

import io.reactivex.Observable
import javax.inject.Inject

class UserRepository @Inject internal constructor(private val userDao: UserDao) : UserRepo {

    override fun insertUser(user: User): Observable<Boolean> {
        userDao.insert(user)
        return Observable.just(true)
    }

    override fun loadUserById(id: Int): Observable<User> = Observable.fromCallable { userDao.loadById(id) }
}