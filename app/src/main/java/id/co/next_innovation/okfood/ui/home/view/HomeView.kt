package id.co.next_innovation.okfood.ui.home.view

import id.co.next_innovation.okfood.data.database.model.GroupMenu
import id.co.next_innovation.okfood.data.database.model.Type
import id.co.next_innovation.okfood.ui.base.view.MVPView

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */
interface HomeView: MVPView {

    fun onViewPrepared()
    fun showGroupMenu(groupMenus: List<GroupMenu>?)
    fun showType(types: List<Type>)
}