package id.co.next_innovation.okfood.ui.home.interactor

import id.co.next_innovation.okfood.data.network.reponse.GroupMenuResponse
import id.co.next_innovation.okfood.data.network.reponse.TypeResponse
import id.co.next_innovation.okfood.ui.base.interactor.MVPInteractor
import id.co.next_innovation.okfood.ui.home.view.HomeView
import io.reactivex.Observable

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

interface HomeMvpInteractor : MVPInteractor {

    fun getGroupMenu(): Observable<GroupMenuResponse>
    fun getType() : Observable<TypeResponse>
    fun setLat(lat: Double?)
    fun setLon(lon: Double?)
}