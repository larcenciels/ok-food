package id.co.next_innovation.okfood.ui.home.interactor

import android.content.Context
import id.co.next_innovation.okfood.data.network.ApiHelper
import id.co.next_innovation.okfood.data.network.reponse.GroupMenuResponse
import id.co.next_innovation.okfood.data.network.reponse.TypeResponse
import id.co.next_innovation.okfood.data.preferences.PreferenceHelper
import id.co.next_innovation.okfood.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

class HomeInteractor @Inject constructor(private val mContext: Context, preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), HomeMvpInteractor {

    override fun getGroupMenu(): Observable<GroupMenuResponse> = apiHelper.getGroupMenus()
    override fun getType(): Observable<TypeResponse> = apiHelper.getTypes()
    override fun setLat(lat: Double?) = preferenceHelper.setTmpLat(lat)
    override fun setLon(lon: Double?) = preferenceHelper.setTmpLon(lon)
}