/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.home.presenter

import id.co.next_innovation.okfood.ui.base.presenter.MVPPresenter
import id.co.next_innovation.okfood.ui.home.interactor.HomeMvpInteractor
import id.co.next_innovation.okfood.ui.home.view.HomeView

interface HomeMvpPresenter<V: HomeView, I: HomeMvpInteractor> : MVPPresenter<V, I> {

    fun onViewPrepared()
    fun onPreparedType()
    fun saveLatLng(lat: Double?, lon: Double?)
}