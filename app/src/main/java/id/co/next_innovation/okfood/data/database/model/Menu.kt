/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.database.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Menu internal constructor(
        @SerializedName("id")
        @Expose
        var id: Int,
        @SerializedName("name")
        @Expose
        var name: String ,
        @SerializedName("category_id")
        @Expose
        var categoryId: Int ,
        @SerializedName("description")
        @Expose
        var description: String ,
        @SerializedName("price")
        @Expose
        var price: String ,
        @SerializedName("image")
        @Expose
        var image: String ,
        @SerializedName("availability")
        @Expose
        var availability: Int ,
        @SerializedName("rating")
        @Expose
        var rating: Float ,
        @SerializedName("created_at")
        @Expose
        var createdAt: String
)