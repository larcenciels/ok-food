package id.co.next_innovation.okfood.ui.login.interactor

import android.content.Context
import id.co.next_innovation.okfood.data.database.repository.user.User
import id.co.next_innovation.okfood.data.database.repository.user.UserRepo
import id.co.next_innovation.okfood.data.network.ApiHelper
import id.co.next_innovation.okfood.data.network.reponse.UserResponse
import id.co.next_innovation.okfood.data.network.request.UserRequest
import id.co.next_innovation.okfood.data.preferences.PreferenceHelper
import id.co.next_innovation.okfood.ui.base.interactor.BaseInteractor
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */
class LoginInteractor @Inject constructor(private val userRepoHelper: UserRepo, preferenceHelper: PreferenceHelper, apiHelper: ApiHelper) : BaseInteractor(preferenceHelper, apiHelper), LoginMvpInteractor {

    override fun performSignIn(email: String, password: String): Observable<UserResponse.Login> =
        apiHelper.performSignIn(UserRequest.Login(email, password))

    override fun updateUserLoggedIn(user: User) {
        userRepoHelper.insertUser(user)
        preferenceHelper.setLoggedIn(true)
    }

}