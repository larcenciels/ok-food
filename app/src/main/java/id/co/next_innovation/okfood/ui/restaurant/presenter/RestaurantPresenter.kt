/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.restaurant.presenter

import id.co.next_innovation.okfood.ui.base.presenter.BasePresenter
import id.co.next_innovation.okfood.ui.restaurant.interactor.RestaurantMvpInteractor
import id.co.next_innovation.okfood.ui.restaurant.view.RestaurantView
import id.co.next_innovation.okfood.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RestaurantPresenter<V : RestaurantView, I : RestaurantMvpInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) :
        BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable),
        RestaurantMvpPresenter<V, I> {

    override fun getLat(): Double {
        return interactor.let {
            it!!.getLat()
        }
    }

    override fun getLon(): Double {
        return interactor.let {
            it!!.getLon()
        }
    }
    override fun onViewPrepared() {
        getView()?.showProgress()
        interactor?.let {
            compositeDisposable.add(it.getRestaurants()
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ restaurantResponse ->
                        if (restaurantResponse.data.restaurant.isNotEmpty()) {
                            getView()?.let {
                                it.hideProgress()
                                it.showRestaurants(restaurantResponse.data.restaurant)
                            }
                        }
                    }))
        }
    }
}