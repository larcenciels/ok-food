/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.account

import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.account.interactor.AccountInteractor
import id.co.next_innovation.okfood.ui.account.interactor.AccountMvpInteractor
import id.co.next_innovation.okfood.ui.account.presenter.AccountMvpPresenter
import id.co.next_innovation.okfood.ui.account.presenter.AccountPresenter
import id.co.next_innovation.okfood.ui.account.view.AccountView

@Module
class AccountActivityModule {

    @Provides
    internal fun provideAccountInteractor(accountInteractor: AccountInteractor) : AccountMvpInteractor = accountInteractor

    @Provides
    internal fun provideAccountPresenter(accountPresenter: AccountPresenter<AccountView, AccountMvpInteractor>):
            AccountMvpPresenter<AccountView, AccountMvpInteractor> = accountPresenter
}