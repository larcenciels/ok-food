package id.co.next_innovation.okfood.ui.splash.view

import id.co.next_innovation.okfood.ui.base.view.MVPView

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

interface SplashView : MVPView {

    fun gotoHomeActivity()

    fun gotoLoginActivity()
}