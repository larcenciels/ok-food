package id.co.next_innovation.okfood.ui.splash.presenter

import id.co.next_innovation.okfood.ui.base.presenter.MVPPresenter
import id.co.next_innovation.okfood.ui.splash.interactor.SplashMvpInteractor
import id.co.next_innovation.okfood.ui.splash.view.SplashView

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

interface SplashMvpPresenter<V : SplashView, I : SplashMvpInteractor> : MVPPresenter<V, I> {

    fun decideActivityToGo(): Unit?
}