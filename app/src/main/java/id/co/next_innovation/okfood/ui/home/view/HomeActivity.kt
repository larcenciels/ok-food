/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.home.view

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v4.widget.NestedScrollView
import android.support.v7.app.ActionBar
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.GridLayout
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.nabinbhandari.android.permissions.PermissionHandler
import com.yarolegovich.discretescrollview.DiscreteScrollView
import com.yarolegovich.lovelydialog.LovelyStandardDialog
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.home.interactor.HomeMvpInteractor
import id.co.next_innovation.okfood.ui.home.presenter.HomeMvpPresenter
import javax.inject.Inject
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.data.database.model.GroupMenu
import id.co.next_innovation.okfood.data.database.model.Type
import id.co.next_innovation.okfood.ui.helper.GridSpacingItemDecorator
import id.co.next_innovation.okfood.ui.helper.slider.Banner
import id.co.next_innovation.okfood.ui.helper.slider.DemoBanner
import id.co.next_innovation.okfood.ui.history.view.HistoryActivity
import id.co.next_innovation.okfood.ui.home.view.adapter.GroupMenuAdapter
import id.co.next_innovation.okfood.ui.home.view.adapter.TypeAdapter
import id.co.next_innovation.okfood.ui.search.view.SearchActivity
import id.co.next_innovation.okfood.ui.social.view.SocialActivity
import kotlinx.android.synthetic.main.activity_home.*
import java.security.Permissions
import java.util.ArrayList


class HomeActivity : BaseActivity(), HomeView, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<LocationSettingsResult>, LocationListener {


    @Inject
    lateinit var presenter: HomeMvpPresenter<HomeView, HomeMvpInteractor>

    @Inject
    lateinit var groupMenuAdapter: GroupMenuAdapter

    @Inject
    lateinit var typeAdapter: TypeAdapter

    lateinit var discreteScrollView: DiscreteScrollView

    lateinit var mGoogleApiClient: GoogleApiClient
    lateinit var mLocationRequest: LocationRequest
    lateinit var mLocationSettingsRequest: LocationSettingsRequest
    var mCurrentLocation: Location? = null

    val KEY_REQUESTING_LOCATION_UPDATES: String = "requesting-location-updates"
    val KEY_LOCATION: String = "location"
    val KEY_LAST_UPDATED_TIME_STRING: String = "last-updated-time-string"
    val REQUEST_CHECK_SETTINGS = 2018

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        presenter.onAttach(this)


        buildGoogleApiClient()
        setupDiscreteScrollview()
        setupToolbar()
        checkLocationPermission()

        onViewPrepared()
        //showScrollHelper() //todo : the layout

        searchbar.setOnClickListener({ startActivity(Intent(this, SearchActivity::class.java)) })

        ViewCompat.setNestedScrollingEnabled(recycler_parent_resto_category, false)
        ViewCompat.setNestedScrollingEnabled(recycler_types, false)

    }

    private fun setupToolbar() {
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setIcon(R.drawable.logo_go_food)
        actionBar?.setDisplayShowHomeEnabled(true)
        actionBar?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        actionBar?.setLogo(R.drawable.logo_go_food)
    }

    override fun onViewPrepared() {
        recycler_parent_resto_category.layoutManager = GridLayoutManager(this, 3, GridLayout.VERTICAL, false)
        recycler_parent_resto_category.itemAnimator = DefaultItemAnimator()
        recycler_parent_resto_category.adapter = groupMenuAdapter
        recycler_parent_resto_category.addItemDecoration(GridSpacingItemDecorator(3, 10, false))

        recycler_types.layoutManager = GridLayoutManager(this, 2, GridLayout.VERTICAL, false)
        recycler_types.itemAnimator = DefaultItemAnimator()
        recycler_types.adapter = typeAdapter

        presenter.onViewPrepared()
        presenter.onPreparedType()
    }

    private fun setupDiscreteScrollview() {
        val banner: List<Banner>
        val demoBanner: DemoBanner = DemoBanner.get()
        banner = demoBanner.banner

        discreteScrollView = findViewById(R.id.discrete_scrollview)
        discreteScrollView.adapter = id.co.next_innovation.okfood.ui.helper.slider.SliderAdapter(banner)
        discreteScrollView.setOffscreenItems(2)
        discreteScrollView.setItemTransitionTimeMillis(10)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.food_home_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            R.id.action_fav -> startActivity(Intent(this, SocialActivity::class.java))
            R.id.action_basket -> startActivity(Intent(this, HistoryActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showGroupMenu(groupMenus: List<GroupMenu>?) {
        groupMenus?.let {
            groupMenuAdapter.addGroupMenuToList(groupMenus)
        }
    }

    override fun showType(types: List<Type>) {
        types.let {
            typeAdapter.addTypeToList(it)
        }
    }

    private fun showScrollHelper() {
        if (nested_scrollview != null) {
            nested_scrollview.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                if (scrollY > oldScrollY) {
                    Log.e("SEE MORE", "SEE MORE VISIBLE")
                }
                if (scrollY == (v.getChildAt(0).measuredHeight - v.measuredHeight)) {
                    Log.e("SEE MORE", "SEE MORE GONE")
                }
            })
        }
    }

    fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 50 * 1000
        mLocationRequest.fastestInterval = 7 * 1000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    fun buildLocationSettingsRequest() {
        val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()
    }

    fun checkLocationSettings() {
        val result: PendingResult<LocationSettingsResult>? = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mLocationSettingsRequest)
        result?.setResultCallback(this)
    }

    fun checkLocationPermission() {

        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            com.nabinbhandari.android.permissions.Permissions.check(this, android.Manifest.permission.ACCESS_FINE_LOCATION,
                    null,
                    object : PermissionHandler() {
                        override fun onGranted() {
                            buildGoogleApiClient()
                            createLocationRequest()
                            buildLocationSettingsRequest()
                            checkLocationSettings()
                        }

                        override fun onBlocked(context: Context?, blockedList: ArrayList<String>?): Boolean {
                            //return super.onBlocked(context, blockedList)
                            finish()
                            return false
                        }

                        override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                            super.onDenied(context, deniedPermissions)
                            finish()
                        }
                    })
        } else {
            val locationProviders = Settings.Secure.getString(contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
            if (locationProviders == null || locationProviders == "") {
                buildGoogleApiClient()
                createLocationRequest()
                buildLocationSettingsRequest()
                checkLocationSettings()
            }
        }

    }

    fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()

        mGoogleApiClient.connect()
    }


    override fun onResult(p0: LocationSettingsResult) {
        val status: Status = p0.status
        when (status.statusCode) {
            LocationSettingsStatusCodes.SUCCESS -> Log.e("LOC", "// NO need to show the dialog;")
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.e("LOC", "// Location settings are unavailable so not possible to show any dialog now")
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                //  GPS disabled show the user a dialog to turn it on
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(this, REQUEST_CHECK_SETTINGS)
                } catch (e: IntentSender.SendIntentException) {
                    //failed to show dialog
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_CANCELED -> showNeedLocationDialog() //finish()
            Activity.RESULT_OK -> startLocationUpdates()
        }
    }

    fun startLocationUpdates() {
//        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this).se
    }

    override fun onStart() {
        super.onStart()
        mGoogleApiClient.connect()
    }

    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient.isConnected) {
            stopLocationUpdate()
        }
    }

    override fun onStop() {
        super.onStop()
        mGoogleApiClient.disconnect()
    }

    override fun onConnected(p0: Bundle?) {
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        }
        presenter.saveLatLng(mCurrentLocation?.latitude, mCurrentLocation?.longitude)
    }

    override fun onLocationChanged(p0: Location?) {
        mCurrentLocation = p0

    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun stopLocationUpdate() {

    }

    fun showNeedLocationDialog() {
        LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                .setTopColorRes(R.color.red_dark2)
                .setButtonsColorRes(R.color.black)
                .setIcon(R.drawable.ic_loc)
                .setTitle(R.string.enable_location_service)
                .setMessage(R.string.need_location_service)
                .setPositiveButton(android.R.string.ok, {
                    buildGoogleApiClient()
                    createLocationRequest()
                    buildLocationSettingsRequest()
                    checkLocationSettings()
                })
                .setNegativeButton(android.R.string.no, {
                    finish()
                })
                .show()
    }

    private fun setupSlider() {
//        sliderLayout = findViewById(R.id.slider)
//        val url_maps = HashMap<String, String>()
//        url_maps["Hannibal"] = "https://lelogama.go-jek.com/post_featured_image/blog_mujigae.jpg"
//        url_maps["Big Bang Theory"] = "https://lelogama.go-jek.com/post_featured_image/blog_AW.jpg"
//        url_maps["House of Cards"] = "https://i0.wp.com/d1o6t6wdv45461.cloudfront.net/s4/dev/589c3522746c2.jpeg"
//
//        for (name in url_maps.keys) {
//            val SliderView = DefaultSliderView(this)
//
//            // initialize a SliderLayout
//            SliderView
//                    .description(name)
//                    .image(url_maps.get(name))
//                    .setScaleType(BaseSliderView.ScaleType.Fit)
//
//            //SliderView your extra information
//            SliderView.bundle(Bundle())
//            SliderView.bundle
//                    .putString("extra", name)
//
//            slider.addSlider(SliderView)
//        }
//
//        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Default)
        //sliderLayout.setPresetIndicator(null)
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {
    }
}