/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.restaurant.view

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.maps.model.LatLng
import com.yarolegovich.lovelydialog.LovelyStandardDialog
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.util.extension.loadImage
import id.co.next_innovation.okfood.data.database.model.Restaurant
import id.co.next_innovation.okfood.ui.menu.view.MenuActivity
import id.co.next_innovation.okfood.util.CommonUtil
import kotlinx.android.synthetic.main.adapter_restaurant.view.*
import java.util.*



class RestaurantAdapter(private val restoList: MutableList<Restaurant>) : RecyclerView.Adapter<RestaurantAdapter.RestaurantViewHolder>() {

    lateinit var latitude: String
    lateinit var longitude: String

    override fun getItemCount() = this.restoList.size

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) = holder.let {
        it.clear()
        it.onBind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewRestaurant: Int) = RestaurantViewHolder(LayoutInflater.from(parent.context).inflate(
            R.layout.adapter_restaurant, parent, false))


    internal fun addRestaurantToList(restos: List<Restaurant>, latitude: String, longitude: String) {
        this.restoList.addAll(restos)
        this.latitude = latitude
        this.longitude = longitude
        notifyDataSetChanged()
    }

    inner class RestaurantViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun clear() {
            itemView.img_merchant.setImageDrawable(null)
            itemView.text_merchant_name.text = ""
            itemView.text_merchant_business_hour.text = ""
            itemView.open_close.text = ""
        }

        fun onBind(position: Int) {

            val resto: Restaurant = restoList[position]
            val pos1 = LatLng(latitude.toDouble(), longitude.toDouble())
            val pos2 = LatLng(resto.latitude, resto.longitude)
            val dist: Double = CommonUtil.distance(pos1, pos2)

            itemView.text_merchant_name.text = resto.name
            itemView.text_merchant_business_hour.text = "Open "+resto.open +" - "+ resto.close

            itemView.text_merchant_address.text = resto.street + ", "+resto.district + ", "+resto.city

            val currentTime = Calendar.getInstance()

            if (isTimeBetweenTwoHours(resto.open.replace(":", "").toInt()/10000, resto.close.replace(":", "").toInt()/10000, currentTime)) {
                itemView.img_merchant.loadImage(resto.image)
                itemView.open_close.text = "OPEN"
                itemView.open_close.setTextColor(itemView.resources.getColor(R.color.color_open))
                itemView.layout_opened_closed.background = itemView.resources.getDrawable(R.drawable.food_button_stroke_green)
            } else if (!isTimeBetweenTwoHours(resto.open.replace(":", "").toInt()/10000, resto.close.replace(":", "").toInt()/10000, currentTime)){
                itemView.img_merchant.loadImage(resto.image)
                itemView.open_close.text = "CLOSED"
                itemView.open_close.setTextColor(itemView.resources.getColor(R.color.color_closed))
                itemView.layout_opened_closed.background = itemView.resources.getDrawable(R.drawable.food_button_stroke_red)
                itemView.layout_img_merchant.setBackgroundColor(itemView.resources.getColor(R.color.trans_80))
            }
            setItemClickListener(resto, resto.id, resto.name)
        }

        private fun setItemClickListener(resto: Restaurant?, id: Int?, name: String?) {
            itemView.setOnClickListener {
                id?.let {
                    val pos1 = LatLng(latitude.toDouble(), longitude.toDouble())
                    val pos2 = LatLng(resto!!.latitude, resto.longitude)
                    val dist: Double = CommonUtil.distance(pos1, pos2)

                    if (dist>25.00) {
                        Toast.makeText(itemView.context, "JARAK TERLALU JAUH", Toast.LENGTH_LONG).show()
                    }

                    val intent = Intent(itemView.context, MenuActivity::class.java)
                    intent.putExtra("restaurant_name", name)
                    intent.putExtra("restaurant_id", id)
                    intent.putExtra("restaurant", resto)
                    itemView.context.startActivity(intent)
                }
            }
        }

    }

    /**
     * @return true if Current Time is between fromHour and toHour
     */
    fun isTimeBetweenTwoHours(fromHour: Int, toHour: Int, now: Calendar): Boolean {
        //Start Time
        val from = Calendar.getInstance()
        from.set(Calendar.HOUR_OF_DAY, fromHour)
        from.set(Calendar.MINUTE, 0)
        //Stop Time
        val to = Calendar.getInstance()
        to.set(Calendar.HOUR_OF_DAY, toHour)
        to.set(Calendar.MINUTE, 0)

        if (to.before(from)) {
            if (now.after(to))
                to.add(Calendar.DATE, 1)
            else
                from.add(Calendar.DATE, -1)
        }
        return now.after(from) && now.before(to)
    }


}