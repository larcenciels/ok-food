package id.co.next_innovation.okfood.ui.login.presenter

import id.co.next_innovation.okfood.ui.base.presenter.MVPPresenter
import id.co.next_innovation.okfood.ui.login.interactor.LoginMvpInteractor
import id.co.next_innovation.okfood.ui.login.view.LoginView

/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

interface LoginMvpPresenter<V: LoginView, I: LoginMvpInteractor> : MVPPresenter<V, I> {

    fun signIn(email: String, password: String)
}
