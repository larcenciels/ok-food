package id.co.next_innovation.okfood.di

import javax.inject.Qualifier

/**
 * Created by jyotidubey on 22/01/18.
 */
@Qualifier
@Retention annotation class ApiKeyInfo