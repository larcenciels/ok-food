package id.co.next_innovation.okfood.ui.menu.item.interactor

import id.co.next_innovation.okfood.data.network.reponse.MenuResponse
import id.co.next_innovation.okfood.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface ItemMvpInteractor : MVPInteractor {

    fun getRestaurantMenus(categoryId: Int): Observable<MenuResponse>
}