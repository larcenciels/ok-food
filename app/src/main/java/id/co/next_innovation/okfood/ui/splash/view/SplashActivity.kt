package id.co.next_innovation.okfood.ui.splash.view

import android.os.Bundle
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.splash.interactor.SplashMvpInteractor
import id.co.next_innovation.okfood.ui.splash.presenter.SplashMvpPresenter
import javax.inject.Inject
import android.content.Intent
import android.os.Handler
import id.co.next_innovation.okfood.ui.home.view.HomeActivity
import id.co.next_innovation.okfood.ui.login.view.LoginActivity


/**
 * Copyright 2017 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, September 2017
 */

class SplashActivity : BaseActivity(), SplashView {

    @Inject
    lateinit var presenter: SplashMvpPresenter<SplashView, SplashMvpInteractor>

    private val SPLASH: Long = 3000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter.onAttach(this)

        Handler().postDelayed({
            presenter.decideActivityToGo()
        }, SPLASH)
    }

    override fun gotoHomeActivity() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun gotoLoginActivity() {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {

    }
}