/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu.item

import android.support.v7.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.menu.item.interactor.ItemInteractor
import id.co.next_innovation.okfood.ui.menu.item.interactor.ItemMvpInteractor
import id.co.next_innovation.okfood.ui.menu.item.presenter.ItemMvpPresenter
import id.co.next_innovation.okfood.ui.menu.item.presenter.ItemPresenter
import id.co.next_innovation.okfood.ui.menu.item.view.ItemAdapter
import id.co.next_innovation.okfood.ui.menu.item.view.ItemFragment
import id.co.next_innovation.okfood.ui.menu.item.view.ItemView

@Module
class ItemFragmentModule {
    @Provides
    internal fun provideItemInteractor(interactor: ItemInteractor): ItemMvpInteractor = interactor

    @Provides
    internal fun provideItemPresenter(presenter: ItemPresenter<ItemView, ItemMvpInteractor>)
            : ItemMvpPresenter<ItemView, ItemMvpInteractor> = presenter

    @Provides
    internal fun provideBlogAdapter(): ItemAdapter = ItemAdapter(ArrayList())


    @Provides
    internal fun provideLinearLayoutManager(fragment: ItemFragment): LinearLayoutManager = LinearLayoutManager(fragment.activity)


}