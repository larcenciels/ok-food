/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.account.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.ActionBar
import id.co.next_innovation.okfood.BuildConfig
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.ui.account.interactor.AccountMvpInteractor
import id.co.next_innovation.okfood.ui.account.presenter.AccountMvpPresenter
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import kotlinx.android.synthetic.main.activity_account.*
import javax.inject.Inject

class AccountActivity: BaseActivity(), AccountView {

    @Inject
    lateinit var presenter: AccountMvpPresenter<AccountView, AccountMvpInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        presenter.onAttach(this)

        setup()

        llRateApp.setOnClickListener {
            val packagename: String = packageName
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+packagename)))
            }catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id="+packagename)))
            }
        }
    }

    fun setup() {
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayShowTitleEnabled(true)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.title = resources.getString(R.string.my_account)
        versionInfo.text = "v"+BuildConfig.VERSION_NAME
    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}