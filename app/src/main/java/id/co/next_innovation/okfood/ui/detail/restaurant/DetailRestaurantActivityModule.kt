/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.detail.restaurant

import android.support.v7.widget.LinearLayoutManager
import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.detail.restaurant.interactor.DetailRestaurantInteractor
import id.co.next_innovation.okfood.ui.detail.restaurant.interactor.DetailRestaurantMvpInteractor
import id.co.next_innovation.okfood.ui.detail.restaurant.presenter.DetailRestaurantMvpPresenter
import id.co.next_innovation.okfood.ui.detail.restaurant.presenter.DetailRestaurantPresenter
import id.co.next_innovation.okfood.ui.detail.restaurant.view.DayAdapter
import id.co.next_innovation.okfood.ui.detail.restaurant.view.DetailRestaurantActivity
import id.co.next_innovation.okfood.ui.detail.restaurant.view.DetailRestaurantView
import id.co.next_innovation.okfood.ui.restaurant.view.RestaurantActivity

@Module
class DetailRestaurantActivityModule {

    @Provides
    internal fun provideDetailRestaurantInteractor(detailRestaurantInteractor: DetailRestaurantInteractor): DetailRestaurantMvpInteractor = detailRestaurantInteractor

    @Provides
    internal fun provideDetailRestaurantPresenter(detailRestaurantPresenter: DetailRestaurantPresenter<DetailRestaurantView, DetailRestaurantMvpInteractor>):
            DetailRestaurantMvpPresenter<DetailRestaurantView, DetailRestaurantMvpInteractor> =  detailRestaurantPresenter

    @Provides
    internal fun provideDayAdapter(): DayAdapter = DayAdapter(ArrayList())


    @Provides
    internal fun provideLinearLayoutManager(activity: DetailRestaurantActivity): LinearLayoutManager = LinearLayoutManager(activity)
}