/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu.item

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.co.next_innovation.okfood.ui.menu.item.view.ItemFragment

@Module
internal abstract class ItemFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(ItemFragmentModule::class))
    internal abstract fun provideItemFragmentFactory(): ItemFragment
}