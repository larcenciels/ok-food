/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.database.model

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable

data class Category internal constructor(

        @SerializedName("id")
        @Expose
        var id: Int,
        @SerializedName("name")
        @Expose
        var name: String ,
        @SerializedName("restaurant_id")
        @Expose
        var restaurantId: Int ,
        @SerializedName("created_at")
        @Expose
        var createdAt: String

) : Serializable {
        companion object {
                @SerializedName("name")
                @Expose
                var name: String? =null
        }
}