/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.home.presenter

import android.util.Log
import com.google.gson.Gson
import id.co.next_innovation.okfood.ui.base.presenter.BasePresenter
import id.co.next_innovation.okfood.ui.home.interactor.HomeMvpInteractor
import id.co.next_innovation.okfood.ui.home.view.HomeView
import id.co.next_innovation.okfood.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

class HomePresenter<V: HomeView, I: HomeMvpInteractor>
    @Inject
    internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) :
        BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable),
        HomeMvpPresenter<V, I> {

    override fun onViewPrepared() {
        getView()?.showProgress()
        interactor?.let {
            it.getGroupMenu()
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ groupMenuResponse ->
                        getView()?.let {
                            it.hideProgress()
                            it.showGroupMenu(groupMenuResponse.data.groupMenu)
                        }
                    },  { err -> Timber.e(err) })
        }
    }

    override fun onPreparedType() {
        getView()?.showProgress()
        interactor?.let {
            it.getType()
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ typeResponse ->
                        getView()?.let {
                            it.showType(typeResponse.data.types)
                            Log.e("LOG ON PRESENTER", Gson().toJson(typeResponse.data))
                        }
                    })
        }
    }

    override fun saveLatLng(lat: Double?, lon: Double?) {
        interactor?.let {
            it.setLat(lat)
            it.setLon(lon)
        }
    }
}

