package id.co.next_innovation.okfood.ui.base.view

/**
 * Created by jyotidubey on 04/01/18.
 */
interface MVPView {

    fun showProgress()

    fun hideProgress()

}