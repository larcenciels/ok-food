/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.menu.interactor

import id.co.next_innovation.okfood.data.network.reponse.CategoryResponse
import id.co.next_innovation.okfood.ui.base.interactor.MVPInteractor
import io.reactivex.Observable

interface MenuMvpInteractor: MVPInteractor {

    fun getLat(): Double
    fun getLon(): Double

    fun getCategoriesMenuRestaurant(restaurantId: Int): Observable<CategoryResponse>
}