/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.search

import dagger.Module
import dagger.Provides
import id.co.next_innovation.okfood.ui.search.interactor.SearchInteractor
import id.co.next_innovation.okfood.ui.search.interactor.SearchMvpInteractor
import id.co.next_innovation.okfood.ui.search.presenter.SearchMvpPresenter
import id.co.next_innovation.okfood.ui.search.presenter.SearchPresenter
import id.co.next_innovation.okfood.ui.search.view.SearchView

@Module
class SearchActivityModule {

    @Provides
    internal fun provideSearchInteractor(searchInteractor: SearchInteractor): SearchMvpInteractor = searchInteractor

    @Provides
    internal fun provideSearchPresenter(searchPresenter: SearchPresenter<SearchView, SearchMvpInteractor>)
                    :SearchMvpPresenter<SearchView, SearchMvpInteractor> =  searchPresenter
}