/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.data.database.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Restaurant internal constructor(
        @SerializedName("id")
        @Expose
        var id: Int,
        @SerializedName("name")
        @Expose
        var name: String? = null,
        @SerializedName("type_id")
        @Expose
        var typeId: Int? = null,
        @SerializedName("merchant_id")
        @Expose
        var merchantId: Int? = null,
        @SerializedName("group_menu_id")
        @Expose
        var groupMenuId: Int? = null,
        @SerializedName("description")
        @Expose
        var description: String? = null,
        @SerializedName("city")
        @Expose
        var city: String? = null,
        @SerializedName("district")
        @Expose
        var district: String? = null,
        @SerializedName("street")
        @Expose
        var street: String? = null,
        @SerializedName("contact")
        @Expose
        var contact: String? = null,
        @SerializedName("image")
        @Expose
        var image: String,
        @SerializedName("latitude")
        @Expose
        var latitude: Double,
        @SerializedName("longitude")
        @Expose
        var longitude: Double,
        @SerializedName("rating")
        @Expose
        var rating: Int? = null,
        @SerializedName("is_active")
        @Expose
        var isActive: Int? = null,
        @SerializedName("priority")
        @Expose
        var priority: Int? = null,
        @SerializedName("open")
        @Expose
        var open: String,
        @SerializedName("close")
        @Expose
        var close: String
): Serializable