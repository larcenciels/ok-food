/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.history.presenter

import id.co.next_innovation.okfood.ui.base.presenter.MVPPresenter
import id.co.next_innovation.okfood.ui.history.interactor.HistoryMvpInteractor
import id.co.next_innovation.okfood.ui.history.view.HistoryView

interface HistoryMvpPresenter<V: HistoryView, I: HistoryMvpInteractor> : MVPPresenter<V, I>