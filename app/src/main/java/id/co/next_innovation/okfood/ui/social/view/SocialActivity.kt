/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.social.view

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.ui.account.view.AccountActivity
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.social.interactor.SocialMvpInteractor
import id.co.next_innovation.okfood.ui.social.presenter.SocialMvpPresenter
import kotlinx.android.synthetic.main.activity_social.*
import kotlinx.android.synthetic.main.food_layout_social_toolbar.*
import javax.inject.Inject

class SocialActivity: BaseActivity(), SocialView {

    @Inject
    lateinit var presenter: SocialMvpPresenter<SocialView, SocialMvpInteractor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_social)
        presenter.onAttach(this)

        setup()

        text_food_feed.setOnClickListener {
            setNonActive()
            text_food_feed.background = resources.getDrawable(R.drawable.food_selector_social_tab)
            text_food_feed.setTextColor(resources.getColor(android.R.color.white))
        }

        text_food_my_favourites.setOnClickListener {
            setNonActive()
            text_food_my_favourites.background = resources.getDrawable(R.drawable.food_selector_social_tab)
            text_food_my_favourites.setTextColor(resources.getColor(android.R.color.white))
        }
    }

    fun setup() {

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = " "
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_social, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_user -> startActivity(Intent(this, AccountActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    fun setNonActive() {
        text_food_my_favourites.background = resources.getDrawable(R.drawable.food_selector_social_tab_for_white)
        text_food_my_favourites.setTextColor(resources.getColor(android.R.color.black))
        text_food_feed.background = resources.getDrawable(R.drawable.food_selector_social_tab_for_white)
        text_food_feed.setTextColor(resources.getColor(android.R.color.black))
    }

    override fun onFragmentAttached() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFragmentDetached(tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}