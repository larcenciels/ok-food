/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.history.presenter

import id.co.next_innovation.okfood.ui.base.presenter.BasePresenter
import id.co.next_innovation.okfood.ui.history.interactor.HistoryMvpInteractor
import id.co.next_innovation.okfood.ui.history.view.HistoryView
import id.co.next_innovation.okfood.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class HistoryPresenter<V: HistoryView, I: HistoryMvpInteractor>
@Inject internal constructor(interactor: I, schedulerProvider: SchedulerProvider, disposable: CompositeDisposable) :
        BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = disposable),
        HistoryMvpPresenter<V, I> {

}