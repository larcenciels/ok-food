/**
 * Copyright 2018 Winnerawan T
 * Unauthorized copying of this file, via any medium is strictly
 * prohibited Proprietary and confidential
 * Written by Winnerawan T <winnerawan@gmail.com>, March 2018
 */

package id.co.next_innovation.okfood.ui.restaurant.view

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.ActionBar
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import id.co.next_innovation.okfood.R
import id.co.next_innovation.okfood.data.database.model.Restaurant
import id.co.next_innovation.okfood.ui.base.view.BaseActivity
import id.co.next_innovation.okfood.ui.history.view.HistoryActivity
import id.co.next_innovation.okfood.ui.restaurant.interactor.RestaurantMvpInteractor
import id.co.next_innovation.okfood.ui.restaurant.presenter.RestaurantMvpPresenter
import id.co.next_innovation.okfood.ui.search.view.SearchActivity
import id.co.next_innovation.okfood.ui.social.view.SocialActivity
import kotlinx.android.synthetic.main.activity_restaurant.*
import javax.inject.Inject

class RestaurantActivity : BaseActivity(), RestaurantView {

    @Inject
    lateinit var presenter: RestaurantMvpPresenter<RestaurantView, RestaurantMvpInteractor>

    @Inject
    lateinit var restaurantAdapter: RestaurantAdapter

    @Inject
    lateinit var layoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant)
        presenter.onAttach(this)

        setupToolbar()
        onViewPrepared()

        searchbar.setOnClickListener { startActivity(Intent(this, SearchActivity::class.java)) }
    }


    private fun setupToolbar() {

        val bundle: Bundle = intent.extras
        val actionBar: ActionBar? = supportActionBar
        actionBar?.setDisplayShowTitleEnabled(true)
        actionBar?.setDisplayShowHomeEnabled(true)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.title = bundle.getString("name")
        actionBar?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
    }

    override fun onViewPrepared() {
        recycler_restaurant.layoutManager = layoutManager
        recycler_restaurant.adapter = restaurantAdapter
        recycler_restaurant.itemAnimator = DefaultItemAnimator()

        presenter.onViewPrepared()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.food_home_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            R.id.action_fav -> startActivity(Intent(this, SocialActivity::class.java))
            R.id.action_basket -> startActivity(Intent(this, HistoryActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showRestaurants(restaurants: List<Restaurant>?) {
        restaurants?.let {
            restaurantAdapter.addRestaurantToList(it, presenter.getLat().toString(), presenter.getLon().toString())
        }
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(tag: String) {
    }
}