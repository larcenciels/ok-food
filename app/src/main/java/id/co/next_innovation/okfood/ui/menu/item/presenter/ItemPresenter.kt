package id.co.next_innovation.okfood.ui.menu.item.presenter

import id.co.next_innovation.okfood.ui.base.presenter.BasePresenter
import id.co.next_innovation.okfood.ui.menu.item.interactor.ItemMvpInteractor
import id.co.next_innovation.okfood.ui.menu.item.view.ItemView
import id.co.next_innovation.okfood.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ItemPresenter<V: ItemView, I: ItemMvpInteractor> @Inject constructor(interactor: I, schedulerProvider: SchedulerProvider, compositeDisposable: CompositeDisposable) : BasePresenter<V, I>(interactor = interactor, schedulerProvider = schedulerProvider, compositeDisposable = compositeDisposable),
        ItemMvpPresenter<V, I> {

    override fun onViewPrepared(categoryId: Int) {
        getView()?.showProgress()
        interactor?.let {
            compositeDisposable.add(it.getRestaurantMenus(categoryId)
                    .compose(schedulerProvider.ioToMainObservableScheduler())
                    .subscribe({ menuResponse ->
                        getView()?.let {
                            it.hideProgress()
                            it.showMenus(menuResponse.data.menus)
                        }
                    }))
        }
    }
}