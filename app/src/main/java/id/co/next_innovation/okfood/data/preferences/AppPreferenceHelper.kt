package id.co.next_innovation.okfood.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import id.co.next_innovation.okfood.util.AppConstants
import id.co.next_innovation.okfood.di.PreferenceInfo
import javax.inject.Inject

/**
 * Created by jyotidubey on 04/01/18.
 */
class AppPreferenceHelper @Inject constructor(context: Context,
                                              @PreferenceInfo private val prefFileName: String) : PreferenceHelper {
    companion object {
        private val PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE"
        private val PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID"
        private val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
        private val PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME"
        private val PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL"

        private val USER_LOGGEDIN = "PREF_KEY_USER_LOGGEDIN"
        private val USER_LATITUDE = "PREF_KEY_USER_LATITUDE"
        private val USER_LONGITUDE = "PREF_KEY_USER_LONGITUDE"
    }

    private val mPrefs: SharedPreferences = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    override fun getCurrentUserName(): String = mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, "ABC")

    override fun setCurrentUserName(userName: String?) = mPrefs.edit { putString(PREF_KEY_CURRENT_USER_NAME, userName) }

    override fun getCurrentUserEmail(): String = mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, "abc@gmail.com")

    override fun setCurrentUserEmail(email: String?) = mPrefs.edit { putString(PREF_KEY_CURRENT_USER_EMAIL, email) }

    override fun getAccessToken(): String = mPrefs.getString(PREF_KEY_ACCESS_TOKEN, "")

    override fun setAccessToken(accessToken: String?) = mPrefs.edit { putString(PREF_KEY_ACCESS_TOKEN, accessToken) }

    override fun getCurrentUserId(): Long? {
        val userId = mPrefs.getLong(PREF_KEY_CURRENT_USER_ID, AppConstants.NULL_INDEX)
        return when (userId) {
            AppConstants.NULL_INDEX -> null
            else -> userId
        }
    }

    override fun setCurrentUserId(userId: Long?) {
        val id = userId ?: AppConstants.NULL_INDEX
        mPrefs.edit { putLong(PREF_KEY_CURRENT_USER_ID, id) }
    }

    override fun setLoggedIn(loggedIn: Boolean) = mPrefs.edit { putBoolean(USER_LOGGEDIN, loggedIn) }

    override fun isLoggedIn(): Boolean = mPrefs.getBoolean(USER_LOGGEDIN, false)

    override fun setTmpLat(lat: Double?) = mPrefs.edit { putLong(USER_LATITUDE, lat!!.toRawBits()) }

    override fun setTmpLon(lon: Double?) = mPrefs.edit { putLong(USER_LONGITUDE, lon!!.toRawBits()) }

    override fun getTmpLat(): Double = Double.fromBits(mPrefs.getLong(USER_LATITUDE, 0))

    override fun getTmpLon(): Double = Double.fromBits(mPrefs.getLong(USER_LONGITUDE, 0))

}